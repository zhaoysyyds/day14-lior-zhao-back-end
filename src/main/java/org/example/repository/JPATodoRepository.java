package org.example.repository;

import org.example.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JPATodoRepository extends JpaRepository<Todo, Long> {
    List<Todo> findByDone(boolean done);
}
