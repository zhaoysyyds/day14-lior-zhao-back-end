package org.example.service;

import org.example.entity.Todo;
import org.example.exception.TodoNotFoundException;
import org.example.repository.JPATodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    @Autowired
    private JPATodoRepository jpaTodoRepository;

    public List<Todo> getTodoList() {
        return jpaTodoRepository.findAll();
    }

    public Todo createTodo(Todo todo) {
        return jpaTodoRepository.save(todo);
    }

    public void deleteTodo(Long id) {
        jpaTodoRepository.deleteById(id);
    }

    public Todo getTodo(Long id) {
        Optional<Todo> todo = jpaTodoRepository.findById(id);
        if(!todo.isPresent()) {
            throw new TodoNotFoundException();
        }
        return todo.get();
    }

    public Todo updateTodo(Long id, Todo todo) {
        Todo  findTodo = jpaTodoRepository.findById(id).get();
        if(todo.getText() != null){
            findTodo.setText(todo.getText());
        }
        findTodo.setDone(todo.getDone());
        return jpaTodoRepository.save(findTodo);
    }

    public List<Todo> getTodoListByDone(boolean done) {
        return jpaTodoRepository.findByDone(done);
    }
}
