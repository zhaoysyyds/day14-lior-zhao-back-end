package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.entity.Todo;
import org.example.exception.TodoNotFoundException;
import org.example.repository.JPATodoRepository;
import org.example.service.TodoService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Random.class)
public class IntegrationTest {
    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @Autowired
    private TodoService todoService;

    @BeforeEach
    void setUp() {
        jpaTodoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo =new Todo(null,"小白号码",false);
        jpaTodoRepository.save(todo);
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.getDone()));
    }

    @Test
    void should_find_todos_by_done() throws Exception {
        Todo todo =new Todo(null,"小白号码",true);
        jpaTodoRepository.save(todo);
        mockMvc.perform(get("/todos?done=true"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.getDone()));
    }

    @Test
    void should_create_todo() throws Exception {
        Todo todo=new Todo("eeeee");

        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo("小白号码");
        jpaTodoRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertFalse(jpaTodoRepository.findById(todo.getId()).isPresent());
    }


    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo1 = new Todo("小白号码");
        Todo todo2= new Todo("sss");
        jpaTodoRepository.save(todo1);
        jpaTodoRepository.save(todo2);

        mockMvc.perform(get("/todos/{id}", todo1.getId()))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo1.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo1.getText()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo1.getDone()));
    }

    @Test
    void should_update_todo() throws Exception {

        Todo todo= new Todo("xiaobai");
        jpaTodoRepository.save(todo);
        Todo updatingTodo= new Todo("xiaoming");

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(updatingTodo);

        mockMvc.perform(put("/todos/{id}", todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Todo> optionalTodo = jpaTodoRepository.findById(todo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(updatedTodo.getId(), todo.getId());
        Assertions.assertEquals(updatedTodo.getDone(), updatingTodo.getDone());
    }


    @Test
    void should_throw_not_found_exception_when_find_employee_given_employee_id_is_not_exist() throws Exception {
        //given
        Todo todo = new Todo("nihao");
        jpaTodoRepository.save(todo);
        //when
        //then
        mockMvc.perform(get("/todos/1000"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo id not found"));
    }


}
